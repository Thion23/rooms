# *-* coding: latin-1 *-*
import telepot
import prenotazioni
import time
import string

aedo=488770129
nomi={}
username={}

def manda(invio,chat_id,txt):
    inoltro=nomi[chat_id]+" ("+str(chat_id)+")"+" ha scritto:\n"+txt+"\n\nRisposta:\n"+invio
    bot.sendMessage(chat_id,invio,parse_mode="Markdown")
    inoltro+="\n\nUsername: @"+username[chat_id]
    bot.sendMessage(aedo,inoltro)

def identifica(msg,chat_id):
    nome=msg["from"]["first_name"]+" "
    try:
        cognome=msg["from"]["last_name"]
    except:
        cognome=""
    nomi[chat_id]=nome+cognome
    try:
        username[chat_id]=msg["from"]["username"]
    except:
        username[chat_id]=""

def handle(msg):
    content_type, chat_type, chat_id = telepot.glance(msg) #Tipo contenuto, tipo chat, id chat
    identifica(msg, chat_id) #Identifica l'utente (nome e username)
    try:
	g = open("update.log", "r")
    except:
	bot.sendMessage(chat_id, "Mi spiace, non ho ancora avuto accesso alla pagina.")
    else:
	bot.sendMessage(chat_id, "Ultimo aggiornamento: " + g.read())
	bot.sendChatAction(chat_id, "typing")
	g.close()
    print(nomi[chat_id])
    if content_type=="text":
        txt=msg['text']
        if txt=="/start":
            manda("Ciao. Digita l'aula per vedere se oggi e' prenotata.", chat_id, txt)
	elif "/aulelibere" in txt:
		txt=txt.replace("/aulelibere", "")
		bot.sendChatAction(chat_id, "typing")
		manda(prenotazioni.aulelibere(txt),chat_id,msg["text"])
        else:
            bot.sendChatAction(chat_id, "typing")
            if not("/"==txt[0]):
		txt=string.upper(txt)
	    print txt
	    if "AULA" in txt:
		txt=txt.replace("A","",1)
	    elif "LABORATORIO" in txt:
		txt=txt.replace("L","",1)
	    for i in prenotazioni.listaprenotazioni(txt):
                manda(i,chat_id,msg['text'])
    else:
        manda("Non valido",chat_id,content_type)

TOKEN="542966566:AAF5wgN8WnD9UD3Ud2vIeJWUZ9HA5o3CNpc"
bot = telepot.Bot(TOKEN)
bot.message_loop(handle)

while 1:
    time.sleep(10)
