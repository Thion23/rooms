import datetime
import re
import string


def aule():
	return ["Aula C", "Aula F", "Aula G", "Aula E", "Laboratorio M", "Laboratorio T", "Aula Magna Fisica", "Aula Magna Matematica"]


def today():
    return datetime.datetime.strftime(datetime.datetime.today(), "%d/%m/%Y")


def tomorrow():
    return datetime.datetime.strftime(datetime.datetime.today() + datetime.timedelta(1), "%d/%m/%Y")


def regex(txt):
    comand = str.replace(str(txt), "/", "") + "()"
    try:
        return "(?i)\"[A|L].*\", \".*\", \".*\", \".*\", \"" + eval(comand) + " .*\""
    except (NameError, TypeError, SyntaxError):
        return "(?i)\"[AL].*" + txt + ".*\", \".*\", \".*\", \".*\", \"" + today() + " .*\""


def aulelibere(txt):
	if txt=="":
		txt="/today"
	mattina=aule()
	pomeriggio=aule()
	prenotateoggi=cerca(txt)
	for j in aule():
		for k in prenotateoggi:
			i=eval(k)
#			print i[0], j
			if string.lower(j) == string.lower(i[0]):
#				print "orario", i[4][-5:-3]
				if "00"<=i[4][-5:-3]<="12":
					try:
						mattina.remove(j)
					except:
						pass
					if "15"<=i[5][-5:-3]<="24":
						try:
							pomeriggio.remove(j)
						except:
							pass
				if "14"<=i[4][-5:-3]<="24":
					try:
						pomeriggio.remove(j)
						break
					except:
						pass
	if not mattina and not pomeriggio:
		libere.append("Nessun'aula libera.")
	messaggio = "Aule libere "
	if txt=="/today":
		messaggio += "oggi:\n\n*Mattina:*\n"
	else:
		try:
			messaggio += "il " + eval(prenotateoggi[0])[4][0:5] + ":\n\n*Mattina*:\n"
		except IndexError:
			return "Ma cosa hai cercato?"
	for i in mattina:
		messaggio += i + "\n"
	messaggio += "\n*Pomeriggio:*\n"
	for i in pomeriggio:
		messaggio += i + "\n"
	return messaggio

def cerca(txt):
    rooms = open("rooms.txt", "r")
    r = rooms.read()
    rooms.close()
    lista = re.findall(regex(txt), r)
    print lista
    return lista


def listaprenotazioni(txt):
    prenotazione = []
    lista = cerca(txt)
    if not lista:
        return ["Nessuna prenotazione."]
    for i in cerca(txt):
        appoggio = eval(i)
        prenotazione.append("*" + appoggio[0] + "*\n" + appoggio[1] + "\n\nDalle " + appoggio[4][-5:] + " alle " +
                            appoggio[5][-5:] + "\nPrenotato da " + appoggio[2] + " " + appoggio[3] + ".")
    return prenotazione
